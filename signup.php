<?php require "header.php"; ?>
<body>
   <section class="note">
        <div class="primary">
           <?php
            if (isset($_GET['signup']) && $_GET['signup'] == "success"){
            header("Location: ../welcome.php");
          }
      ?>

    <div class="signup">
        <section class="section-defult">
            <form class="signUpForm" action="includes/signup.inc.php" method="post">
                <label for="user">User Name</label>
                <input type="text" name="uname" placeholder="Enter Username" minlength="6" maxlength="20" required >
                <label for="user">Email</label>
                <input type="text" name="mail" placeholder="Enter Email" minlength="15" maxlength="42" required >
                <label for="password">password</label>
                <input type="password" name="pwd" placeholder="Enter Password" minlength="12" maxlength="22" required >
                <label for="confirmPassword"> confirm password</label>
                <input type="password" name="pwdc" placeholder="Enter Password again" minlength="12" maxlength="22" required >
                <button type="submit" name="signupsubmit">Sign up</button>   
            </form>
        </section>
    </div>  
</body>
<?php require "footer.php"; ?>